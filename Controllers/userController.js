// set up dependencies
const User = require ("../models/user.js")
const Vape = require ("../models/vape.js")
const Order = require ("../models/order.js")
const auth = require ("../auth.js")
const bcrypt = require ("bcrypt") // used to encrypt user passwords

// check if the email exists
/*
	1. check for the email
	2. send the result as a response (with error handling)
*/

// it is conventional for the devs to use Boolean in sending return response esp with the backend application
module.exports.checkEmail = (requestBody) => {
	return User.find ({email: requestBody.email}).then ((result, error) => {
		if (error){
			console.log(error)
			return false
		} else {
			if (result.length > 0){
				//  return result
				return true
			} else {
				// return res.send ("email does not exist")
				return false
			}
		}
	})
}

/*
USER REGISTRATION

1. create a new User with the information from the requestBody
2. make sure that the password is encrypted
3. save the new user
*/

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		// hashSync is a function of bcrypt that encrypts the password
			// 10 is the number of rounds/times it runds the algorithm to the reqBody.password
				// max 72 implementations
		password:bcrypt.hashSync(reqBody.password, 10),
		mobileNumber:reqBody.mobileNumber
	})
	return newUser.save().then((saved,error) => {
		if (error){
			console.log(error)
			return false
		} else {
			return true
		}
	})
}

// USER LOGIN
/*
1. find if the email is existing in the database
2. check if the password is correct
*/

module.exports.userLogin = (reqBody) => {
	return User.findOne({email:reqBody.email}).then (result => {
		if (result === null){
			return false
		} else {
			// compareSync function - used to compate a non-encrypted password to an encrypted password and returns a Boolean response depending on the result
			bcrypt.compareSync(reqBody.password, result.password)

			/*
			What should we do after the comparison?
			true - a token should be created since the user is existing and the password is correct
			false - the passwords do not match, thus a token should not be created 
			*/
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result.toObject())}
			// auth - imported auth.js
			// createAccessToken - function inside the auth.js to create access token
			// .toObject - transforms the User into an Object that can be used by our createAccessToken
			} else {
				return false
			}
		}
	})
}



// getProfile
module.exports.getProfile = (data) => {
	return User.findById(data.userId).then (result => {
		if (result === null) {
			return false
		} 
		else {
			result.password = ""
			return result
		}
	})
}



// enrollment
/*module.exports.enroll = async (data) => {
	// finding the user in the database
	let isUserUpdated = await User.findById(data.userId).then(user => {
		// adding the vape to the user's enrollment array
		user.enrollments.push({vape:data.vape})

		return user.save().then ((user,err) =>{
			if (err){
				return false
			} else {
				return true
			}
		})
	})

	let isVapeUpdated = await Vape.findById(data.vape).then(vape => {
		// adding userId in the vape enrollees array
		vape.enrollees.push({userId: data.userId})
		// saving to database
		return vape.save().then((vape, error)=>{
			if(error){
				return false
			}else{
				return true
			}
		})
	})

	if (isUserUpdated && isvapeUpdated){
		return true
	}
	else{
		return false
	}
}
*/


//update admin

module.exports.updateAdmin = ( reqParams, reqBody, data ) => {

return User.findById(data.userId).then (result => {
		if (data.isAdmin == false) {
			return "You are not an admin!"
		}
		else {
	let updatedAdmin = {
		isAdmin: reqBody.isAdmin

	}
	
	return User.findByIdAndUpdate(reqParams.userId, updatedAdmin).then((result, error) => {
		if (error) {
			return false
		} else	{
			return true
		}
			}

	)}
  })
}




