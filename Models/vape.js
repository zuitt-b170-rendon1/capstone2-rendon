const mongoose = require ("mongoose");

const vapeSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Course name is required"]
	},
	description: {
		type: String,
		required: [true, "Course description is required"]
	},
	price:{
		type: Number,
		required: [true, "Price for the course is required"]
		},

	isActive: {
		type: Boolean,
		default: true
	},
	createOn: {
		type: Date,
		default: new Date()
	},
	
/*	purchases:[
	{
		userId:{
			type: String,
			required: [true, "User ID is required"]
		},
		purchasedOn: {
			type: Date,
			default: new Date()
		}
	}]	*/		
})

module.exports = mongoose.model("Vape", vapeSchema)
