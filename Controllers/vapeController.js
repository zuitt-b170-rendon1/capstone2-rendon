const User = require ("../models/user.js")
const Vape = require ("../models/vape.js")
const Order = require ("../models/order.js")
const auth = require ("../auth.js")
const bcrypt = require ("bcrypt") 




module.exports.addVape = (reqBody, userData) => {
	
	return User.findById(userData.userId).then (result => {
		if (userData.isAdmin == false) {
			return "You are not an admin!"
		}
		else {
			let newVape = new Vape ({
		name: reqBody.name,
		description: reqBody.description,
		price:reqBody.price,				
			})
			// 
			return newVape.save().then ((vape,error) => {
				if(error){
					// 
					return false
				} else {
					// Vape creation sucessful
					return "Vape creation sucessful"
				}
			})
		}
	});
}

module.exports.getAllVapes = () => {
	return Vape.find ({}).then (result =>{
		return result
	})
}

/*
using the course id in the url, retrieve a course using a get request
*/

module.exports.getVape = (reqParams) => {
	return Vape.findById(reqParams.vapeId).then (result => {
		return result
	})
}

// retrieve all active vapes
module.exports.getActiveVapes = () => {
	return Vape.find({isActive:true}).then ((result,error) => {
		if (error) {
			console.log(error)
		} else {
			return result
		}
	})
}


// update a course
module.exports.updateVape = ( reqParams, reqBody, userData ) => {

return User.findById(userData.userId).then (result => {
		if (userData.isAdmin == false) {
			return "You are not an admin!"
		}
		else {
	let updatedVape = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	// findByIdAndUpdate - looks for the id of the document (first parameter) and updates it (content of the second parameter)
		//the server will be looking for the id of the reqParams.courseId in the database and updates the document through the content of the object updatedCourse 
	return Vape.findByIdAndUpdate(reqParams.vapeId, updatedVape).then((result, error) => {
		if (error) {
			return false
		} else	{
			return true
		}
			}

	)}
  })
}

// archive a course
/*
controllers business logic: 

    1. create a updateCourse object with the content from the reqBody
            reqBody should have the isActive status of the course to be set to false
    2. find the course by its id and update with the updateCourse object using the findByIdAndUpdate
            handle the errors that may arise
                error/s - false
                no errors - true
*/


// archive a course
module.exports.archiveVape = ( reqParams, reqBody, userData ) => {

	return User.findById(userData.userId).then (result => {
		if (userData.isAdmin === false) {
			return "You are not an admin!"
		}
		else {
	let archivedVape = {
		isActive: reqBody.isActive,
	}
	return Vape.findByIdAndUpdate(reqParams.vapeId, archivedVape).then((result, error) => {
		if (error) {
			return false
		} else	{
			return true
		}
	})
}
})
}

/*
if you do want to requre any resuest body:
module.exports.archiveCourse = ( reqParams ) => {
	let archivedCourse = {
		isActive: false
}
*/