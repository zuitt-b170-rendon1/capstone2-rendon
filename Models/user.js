const mongoose = require ("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	mobileNumber: {
		type: String,
		required: [true, "Mobile number is required"]
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

/*	orders:[
	{
		totalAmount: {
			type: Number,
			required: [true, "Total amount for the order is required"]
		},

		vapeId:{
			type: String,
			required: [true, "Course ID is required"]
		},
		purchasedOn: {
			type: Date,
			default: new Date()
		},
		paid: {
			type: Boolean,
			default: true
		}
	}]*/			
})

module.exports = mongoose.model("User", userSchema)