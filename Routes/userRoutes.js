const express = require("express");
const router = express.Router()

const auth = require ("../auth.js")
const userController = require ("../controllers/userController.js")
const vapeController = require("../controllers/vapeController.js")
const orderController = require("../controllers/orderController.js")


// checking of email
router.post("/checkEmail", (req,res) => {
	userController.checkEmail (req.body).then(resultFromController => res.send (resultFromController))
})

// user registration
router.post ("/register", (req, res) =>{
	userController.registerUser(req.body).then(resultFromController => res.send (resultFromController))
})

// user login
router.post ("/login", (req, res) =>{
	userController.userLogin(req.body).then(resultFromController => res.send (resultFromController))
})

// auth.verify - ensures that a user is logged in before proceeding to the next part of the code; this is the verify function inside the auth.js
router.get("/details", auth.verify, (req, res)=> {
	// decode - decrypts the token inside the authorization (which is in the headers of the request) 
	// req.headers.authorization contains the token that was created for the user
	const userData = auth.decode(req.headers.authorization)
	console.log(userData)
	userController.getProfile( {userId: userData.id} ).then(resultFromController => res.send(resultFromController))
} )


// order
/*router.post("/order", auth.verify, (req, res) => {
	let data = {
		userId:auth.decode(req.headers.authorization).id,
		vapeId: req.body.vapeId
	}
	userController.enroll(data).then(result => res.send(result))
})
*/

//update admin
router.put("/:userId", auth.verify, (req,res,) => {
	const userData = auth.decode(req.headers.authorization)
	userController.updateAdmin(req.params, req.body, userData).then(result => res.send(result))
})

module.exports = router

