// set up dependencies
const User = require ("../models/user.js")
const Vape = require ("../models/vape.js")
const Order = require ("../models/order.js")
const auth = require ("../auth.js")
const bcrypt = require ("bcrypt") // used to encrypt user passwords


//checkout

module.exports.addOrder = (reqBody, orderData) => {
	
	return User.findById(orderData.userId).then (result => {
		if (orderData.isAdmin === true) {
			return "Admin can't do checkout!"
		}
		else {
			let newOrder = new Order ({
		
		vapeId: reqBody.vapeId,
		totalAmount: reqBody.totalAmount,
		userId: reqBody.userId
			})
			return newOrder.save().then ((order,error) => {
				if(error){
					// 
					return false
				} else {
					// Vape creation sucessful
					return "Order sucessful!"
				}
			})
		}
	});
}


/*module.exports.addOrder = (reqBody, orderData) => {
	
	return User.findById(orderData.userId).then (result => {
		if (orderData.isAdmin === true) {
			return "Admin can't do checkout!"
		}
		else {
			let newOrder = new Order ({
		
		vapeId: reqBody.vapeId,
		totalAmount: reqBody.totalAmount

		
			})

			return newOrder.save().then ((order,error) => {
				if(error){
					// 
					return false
				} else {
			

		let isUserUpdated = await User.findById(data.userId).then(user => {
		// adding the courseId to the user's enrollment array
		order.ids.push({vapeId:data.vapeId})

		return user.save().then ((user,err) =>{
			if (err){
				return false
			} else {
				return "Order sucessful!"
			}
		
			})
		}
	)
}})}})}
			*/
		



module.exports.getAllOrders = (userData) => {

	return User.findById(userData.userId).then (result => {
		if (userData.isAdmin == false) {
			return "You are not an admin!"
		}
		else{
	return Order.find ({}).then (result =>{
		return result
	}
	)}
})
}



//retrieve authenticated  user's order
/*module.exports.getOrder = (reqParams) => {
	return Order.find ({}).then (result => {
		return result
	})
}
*/

module.exports.getOrder = ( userData ) => {
	console.log(userData)
	return User.findById(userData.userId).then (result => {
		if (userData.isAdmin === true) {
			return "Invalid User!"
		}
		else{
	return User.find ({userId: userData.userId}).then (result =>{
		return result
	}
	)}
})
}




/*// get specific order

module.exports.getSingleOrder = (reqParams) => {
	return Order.findById(reqParams.orderId).then (result => {
		return result
	})
}
*/










module.exports.deleteOrder = (orderId) => {
	// findByIdAndRemove - finds the item to be deleted and removes it from the database; it used id
	return Order.findByIdAndRemove(orderId).then ((removedOrder,error) => {
		if (error){
			console.log(error);
			return false
		} else {
			return removedOrder
		}
	})
}







/* checkout sample

module.exports.addOrder = (reqBody, orderData) => {
	
	return User.findById(orderData.userId).then (result => {
		if (orderData.isAdmin === true) {
			return "Admin can't do checkout!"
		}
		else {
			let newOrder = new Order ({
		
		vapeId: reqBody.vapeId,
		
			})

			return newOrder.save().then ((order,error) => {
				if(error){
					// 
					return false
				} else {
					// Vape creation sucessful
					return "Order sucessful!"
				}
			})
		}
	});
}
*/










/*// order
module.exports.enroll = async (data) => {
	// finding the user in the database
	let isUserUpdated = await User.findById(data.userId).then(order => {
		// adding the vape to the order array
		// order.ids.push({vapeId:data.vapeId})

		return order.save().then ((user,err) =>{
			if (err){
				return false
			} else {
				return true
			}
		})
	})
}
*/

	/*let isVapeUpdated = await Vape.findById(data.vapeId).then(order => {
		// adding userId in the order array
		order.ids.push({userId: data.userId})
		// saving to database
		return order.save().then((vape, error)=>{
			if(error){
				return false
			}else{
				return true
			}
		})
	})

	if (isUserUpdated && isVapeUpdated){
		return true
	}
	else{
		return false
	}*/

