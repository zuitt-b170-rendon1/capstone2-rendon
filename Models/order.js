const mongoose = require ("mongoose");

const orderSchema = new mongoose.Schema({
	
/*	ids: [{userId:{
		type: String,
		required: [true, "User ID is required"]
		},

	vapeId:{
			type: String,
			required: [true, "Course ID is required"]	
		}
	],
	*/
	totalAmount: {
			type: Number,
			//required: [true, "Total amount for the order is required"]
		},
		
	purchasedOn: {
		type: Date,
		default: new Date()
	},

	userId:{
		type: String,
		required: [true, "User ID is required"]
		},

	vapeId:{
			type: String,
			required: [true, "Course ID is required"]	
		},
	quantity:{
			type: Number,
			default: "1"
					
	}	


	

			
})

module.exports = mongoose.model("Order", orderSchema)