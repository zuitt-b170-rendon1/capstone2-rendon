const express = require("express");
const router = express.Router()

const auth = require ("../auth.js")
const userController = require ("../controllers/userController.js")
const vapeController = require("../controllers/vapeController.js")
const orderController = require("../controllers/orderController.js")



//checkout
router.post ("/checkout", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization) 
	orderController.addOrder(req.body, userData).then(resultFromController => res.send (resultFromController))
})



//get all orders (admin only)
router.get("/", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization)
	orderController.getAllOrders(userData).then(resultFromController => res.send(resultFromController))
})


/*// get single order
router.get("/:orderId", (req,res) => {
	console.log(req.params.orderId)
	orderController.getSingleOrder(req.params).then(result => res.send(result))
})*/


//retrieve authenticated  user's order
router.get("/:userId", auth.verify, (req,res,) => {
	const userData = auth.decode(req.headers.authorization)
	orderController.getOrder(userData).then(resultFromController => res.send(resultFromController))
})


/*
router.get("/details", auth.verify, (req, res)=> {
	const userData = auth.decode(req.headers.authorization)
	console.log(userData)
	orderController.getOrder( {orderId: orderData.id} ).then(resultFromController => res.send(resultFromController))
} )
*/


router.delete("/:id", (req,res) => {
	// URL parameter values are accessed through the request object's "params" property, the specified parameter/property of the objectt will match the URL parameter endpoint
	orderController.deleteOrder(req.params.id).then(result =>res.send(result))
})



module.exports = router