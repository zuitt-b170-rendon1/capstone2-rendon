// setting up dependencies
const express = require("express");
const mongoose = require ("mongoose");
const cors = require ("cors");

// access to routes
const userRoutes = require ("./routes/userRoutes")
const vapeRoutes = require ("./routes/vapeRoutes")
const orderRoutes = require ("./routes/orderRoutes")


// server
const app = express()
const port = process.env.port || 3000

app.use(cors())
//allows all origins/domains to access the backend application

app.use(express.json())
app.use(express.urlencoded({extended:true}))



mongoose.connect("mongodb+srv://crendon:crendon@wdc028-course-booking.6fjk2.mongodb.net/capstone2?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log ("We're connected to the database"))

// defines the routes where the CRUD operations will be executed om the users ("/api/users") and courses ("/api/courses")
app.use("/api/users", userRoutes);
app.use("/api/vapes", vapeRoutes);
app.use("/api/order", orderRoutes);

//app.listen(port, () => console.log(`API now online at port ${port}`));


app.listen( port, () => console.log(`API now online at port ${port}`))
// process.env.PORT works if you are deploying the api in a host like Heroku. this code allows the app to use the environment of that hose (Heroku) in running the server
