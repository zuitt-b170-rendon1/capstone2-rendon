const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const vapeController = require("../controllers/vapeController.js")
const orderController = require("../controllers/orderController.js")


/*
create a route that will let an admin perform addCourse function in the vapeController
	verify that the user is logged in
	decode the token for that user
	use the id, isAdmin, and request body to perform the function in vapeController
		the id and isAdmin are parts of an object 
*/


router.post ("/", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization) //userData wound now contain an object that has token payload (id, emai, isAdmin information of the user)
	vapeController.addVape(req.body, userData).then(resultFromController => res.send (resultFromController))
})

// ecommerce websites

/*
create a route that will retrieve all of our products/courses
	will require login/register functions
*/

router.get("/", (req,res) => {
	vapeController.getAllVapes().then(resultFromController => res.send(resultFromController))
})


// getting all of the documents, in case we need multiple of them, place the route with the criteria ot the find method first
router.get("/active", (req,res) => {
	vapeController.getActiveVapes().then(resultFromController => res.send(resultFromController))
})

// get user's order
router.get("/:vapeId", (req,res) => {
	console.log(req.params.vapeId);
	vapeController.getVape(req.params).then(result => res.send(result))
})




// update a course
router.put("/:vapeId", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization)
	vapeController.updateVape(req.params, req.body, userData).then(result => res.send(result))
})

/*
delete is never a norm in database

use/archiveCourse and send a PUT request to archive course by changing the active status
*/


// archive course
router.put("/:vapeId/archive", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization)
	vapeController.archiveVape(req.params, req.body, userData).then(result => res.send(result))
})

/*
if you dont want to require any request body
vapeController.archiveCourse(req.params, req.body).then(result => res.send(result))
*/

module.exports = router